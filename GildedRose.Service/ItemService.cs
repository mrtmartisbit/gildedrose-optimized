﻿using GildedRose.Data;
using GildedRose.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Service
{
    public class ItemService
    {
        private ItemRepository _itemRepository;

        public ItemService()
        {
            _itemRepository = new ItemRepository();
        }

        public ItemService(ItemRepository itemRepository)
        {
            ItemRepository _itemRepository = itemRepository;
        }

        public List<Item> GetItems()
        {
            return _itemRepository.LoadItems();
        }

    }
}
