﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public abstract class DefaultItem: Item
    {
        public DefaultItem(Item item)
        {
            Name = item.Name;
            Quality = item.Quality;
            SellIn = item.SellIn;
        }
        //I don't think I need to check if it's lower than zero because it's an int
        protected readonly int MinQuality = 0;
        protected int MaxQuality = 50;
        protected int DecrementalQualityValue = 1;
        //default item behavior
        //I could have this in the constructor of each item so when new item gets created it would decrement the values automatically
        //but for more control I created this function to be called explicitly
        public virtual void Degrade()
        {
            SellIn--;
            AjustQuality(DecrementalQualityValue);
            if (SellIn < 0) {
                AjustQuality(DecrementalQualityValue);
            }
        }

        public virtual void AjustQuality(int val)
        {
            if (Quality > MinQuality)
            {
                Quality = Quality - val;
            }
        }
    }
}
