﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public class BackstagePassItem: DefaultItem
    {
        public BackstagePassItem(Item item) : base(item)
        {
        }
        private readonly int ConcertApprochingIn = 11;
        private readonly int ConcertDueIn = 6;
        /// <summary>
        ///   "Backstage passes", like aged brie, increases in Quality as it's SellIn 
        ///     value approaches; Quality increases by 2 when there are 10 days or less
        ///     and by 3 when there are 5 days or less but Quality drops to 0 after the concert
        /// </summary>
        public override void Degrade()
        {
            SellIn--;
            AdjustQuality(DecrementalQualityValue);
            if (SellIn < ConcertApprochingIn)
            {
                AdjustQuality(DecrementalQualityValue);
            }

            if (SellIn < ConcertDueIn)
            {
                AdjustQuality(DecrementalQualityValue);
            }
        }
        
        public void AdjustQuality(int val)
        {
            if (Quality < MaxQuality)
            {
                Quality = Quality + val;
            }
        }
    }
}
