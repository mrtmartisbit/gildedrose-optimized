﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public class NonSpecificItem: DefaultItem
    {
        public NonSpecificItem(Item item): base(item)
        {
        }
    }
}
