﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public class AgedBrieItem : DefaultItem
    {
        public AgedBrieItem(Item item) : base(item)
        {
        }

        public override void Degrade()
        {
            SellIn--;
            AdjustQuality(DecrementalQualityValue);
            if (SellIn < 0)
            {
                AdjustQuality(DecrementalQualityValue);
            }
        }

        public void AdjustQuality(int val)
        {
            if (Quality < MaxQuality)
            {
                Quality = Quality + val;
            }
        }
    }
}
