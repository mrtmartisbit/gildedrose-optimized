﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public class SulfurusItem: DefaultItem
    {
        public SulfurusItem(Item item) : base(item)
        {
            MaxQuality = 80;
        }

        public override void Degrade()
        {
            //is a legendary item and as such its 
            //Quality is 80 and it never alters.
            //sellIn just for record, still decreases
            SellIn--;
        }

        //otherwise if it has been sold we don't need to keep track of it's quality
        //it should be out of inventory
        public void Sell()
        {
            //never has to be sold or decreases in Quality
            Quality = Quality - DecrementalQualityValue;
        }
    }
}
