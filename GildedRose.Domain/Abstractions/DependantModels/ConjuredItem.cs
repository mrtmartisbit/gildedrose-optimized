﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Domain
{
    public class ConjuredItem : DefaultItem
    {
        public ConjuredItem(Item item) : base(item)
        {
            //twice as fast
            DecrementalQualityValue = DecrementalQualityValue * 2;
        }
    }
}
