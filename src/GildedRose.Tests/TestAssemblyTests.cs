using GildedRose.Domain;
using Xunit;

namespace GildedRose.Tests
{
    public class TestAssemblyTests
    {
        [Fact]
        public void TestTheTruth()
        {
            Assert.True(true);
        }

        [Fact]
        public void TestSulfurQualityDoesNotDegrade()
        {
            var expectedQualityResult = 80;
            //assuming they have since the begining of time, so it was never sold
            var item = new Item()
            {
                Name = "Legendary Sulfur",
                SellIn = 1,
                Quality = 80
            };

            var sulfurItem = new SulfurusItem(item);
            sulfurItem.Degrade();

            Assert.NotNull(sulfurItem);
            Assert.Equal(expectedQualityResult, sulfurItem.Quality);
        }

        [Fact]
        public void TestConjuredItem()
        {
            var expectedQualityResult = 20;

            var item = new Item()
            {
                Name = "Conjured test item",
                SellIn = 20,
                Quality = 22
            };

            var conjuredItem = new ConjuredItem(item);

            conjuredItem.Degrade();

            Assert.NotNull(conjuredItem);
            Assert.Equal(expectedQualityResult, conjuredItem.Quality);
        }

        [Fact]
        public void TestAgedBrieItem()
        {
            var expectedQualityResult = 40;

            var item = new Item()
            {
                Name = "Aged Brie",
                SellIn = 1,
                Quality = 39
            };

            var agedBrieItem = new AgedBrieItem(item);
            agedBrieItem.Degrade();

            Assert.NotNull(agedBrieItem);
            Assert.Equal(expectedQualityResult, agedBrieItem.Quality);
        }

        //- Once the sell by date has passed, Quality degrades twice as fast
        [Fact]//or increases twice as fast (I suppose)
        public void TestAgedBrieLateItem()
        {
            var expectedQualityResult = 42;

            var item = new Item()
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 40
            };

            var agedBrieItem = new AgedBrieItem(item);
            agedBrieItem.Degrade();

            Assert.NotNull(agedBrieItem);
            Assert.Equal(expectedQualityResult, agedBrieItem.Quality);
        }

        [Fact]
        public void TestNonSpecificItem()
        {
            var expectedQualityResult = 49;

            var item = new Item()
            {
                Name = "Aged Brie",
                SellIn = 23,
                Quality = 50
            };

            var NonSpecificItem = new NonSpecificItem(item);
            NonSpecificItem.Degrade();

            Assert.NotNull(NonSpecificItem);
            Assert.Equal(expectedQualityResult, NonSpecificItem.Quality);
        }

        [Fact]
        public void TestNonSpecificLateItem()
        {
            var expectedQualityResult = 16;

            var item = new Item()
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 18
            };

            var NonSpecificItem = new NonSpecificItem(item);
            NonSpecificItem.Degrade();

            Assert.NotNull(NonSpecificItem);
            Assert.Equal(expectedQualityResult, NonSpecificItem.Quality);
        }

        [Fact]
        public void TestNonSpecificDegradedItem()
        {
            var expectedQualityResult = 0;

            var item = new Item()
            {
                Name = "Aged Brie",
                SellIn = 0,
                Quality = 0
            };

            var NonSpecificItem = new NonSpecificItem(item);
            NonSpecificItem.Degrade();

            Assert.NotNull(NonSpecificItem);
            Assert.Equal(expectedQualityResult, NonSpecificItem.Quality);
        }

        [Fact]
        public void TestConcertQuality()
        {
            var expectedQualityResult = 21;

            var item = new Item()
            {
                Name = "Very late backstage pass",
                SellIn = 20,
                Quality = 20
            };

            var backstagePassItem = new BackstagePassItem(item);
            backstagePassItem.Degrade();

            Assert.NotNull(backstagePassItem);
            Assert.Equal(expectedQualityResult, backstagePassItem.Quality);
        }

        [Fact]
        public void TestLateConcertQuality()
        {
            var expectedQualityResult = 32;

            var item = new Item()
            {
                Name = "Very late backstage pass",
                SellIn = 10,
                Quality = 30
            };

            var backstagePassItem = new BackstagePassItem(item);
            backstagePassItem.Degrade();

            Assert.NotNull(backstagePassItem);
            Assert.Equal(expectedQualityResult, backstagePassItem.Quality);
        }

        [Fact]
        public void TestVeryLateConcertQuality()
        {
            var expectedQualityResult = 43;

            var item = new Item()
            {
                Name = "Very late backstage pass",
                SellIn = 3,
                Quality = 40
            };

            var backstagePassItem = new BackstagePassItem(item);
            backstagePassItem.Degrade();

            Assert.NotNull(backstagePassItem);
            Assert.Equal(expectedQualityResult, backstagePassItem.Quality);
        }


    }
}