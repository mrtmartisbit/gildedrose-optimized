﻿using GildedRose.Domain;
using GildedRose.Service;
using System.Collections.Generic;

namespace GildedRose.Console
{
    class Program
    {
        public static readonly ItemService _itemService = new ItemService();

        static void Main(string[] args)
        {
            System.Console.WriteLine("OMGHAI!");
            
            IList<Item> Items = _itemService.GetItems();

            DailyUpdateQuality(Items);

            System.Console.ReadKey();

        }

        //Daily - update quality method
        //assuming - this only runs once a day
        public static void DailyUpdateQuality(IList<Item> Items)
        {
            foreach (var item in Items)
            {
                switch (item.Name)
                {
                    case "Sulfuras, Hand of Ragnaros":
                        SulfurusItem _sulfurusitem = new SulfurusItem(item);
                        _sulfurusitem.Degrade();
                        break;
                    case "Aged Brie":
                        AgedBrieItem _agedBrieItem = new AgedBrieItem(item);
                        _agedBrieItem.Degrade();
                        break;
                    case "Backstage passes to a TAFKAL80ETC concert":
                        BackstagePassItem _backstagePassItem = new BackstagePassItem(item);
                        _backstagePassItem.Degrade();
                        break;
                    default:
                        NonSpecificItem _nonSpecificItem = new NonSpecificItem(item);
                        _nonSpecificItem.Degrade();
                        break;
                }
            }
        }
    }
}
